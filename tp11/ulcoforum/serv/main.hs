{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Database.SQLite.Simple
import Web.Scotty
import Lucid
import Data.Text
import TextShow
import qualified Data.Text as T
import GHC.Generics (Generic)
import Control.Monad.IO.Class(liftIO)

data ForumMessage = ForumMessage
    { _fmid :: Int,
      _ftopicid :: Int,
      _fmname :: Text,
      _fmmessage :: Text
    } deriving (Generic, Show)

instance FromRow ForumMessage where
    fromRow = ForumMessage <$> field <*> field <*> field <*> field

data ForumTopic = ForumTopic
    { _ftid :: Int,
      _ftname :: Text
    } deriving (Generic, Show)

instance FromRow ForumTopic where
    fromRow = ForumTopic <$> field <*> field

main :: IO ()
main = scotty 3000 $ do
    get "/" $ html $ renderText (getIndex menu)
    get "/alldata" $ html $ renderText (alldata menu)
        --do
        --topics <- liftIO $ withConnection "ulcoforum.db" dbgetalltopics
        --messages <- liftIO $ withConnection "ulcoforum.db" dbgetallonetopic
        --html $ renderText (alldata menu topics messages)
    get "/alltopics" $ do
        topics <- liftIO $ withConnection "ulcoforum.db" dbgetalltopics
        html $ renderText (alltopics menu topics)
    get "/onetopic/:id" $ do
        id <- param "id"
        topics <- liftIO $ withConnection "ulcoforum.db" dbgetalltopics
        messages <- liftIO $ withConnection "ulcoforum.db" dbgetallonetopic
        let parsedtopic = Prelude.filter (\(ForumTopic ftid _) -> ftid == id) topics
        let parsedmessages = Prelude.filter (\(ForumMessage _ ftid _ _) -> ftid == id) messages
        html $ renderText (oneTopic menu parsedmessages (Prelude.head parsedtopic))

menu :: Html ()
menu = do
    doctype_
    html_ $ do
        head_ $ meta_ [charset_ "utf8"]
        body_ $ do
            a_ [href_ "/"] $ do
                h1_ "ulcoforum"
            a_ [href_ "/alldata"] "Toutes les données"
            span_ $ toHtml $ T.pack " - "
            a_ [href_ "/alltopics"] "Tous les topics"

getIndex :: (Html ()) -> Html ()
getIndex menu = html_ $ do
    menu
    p_ $ toHtml $ T.pack "Bienvenue sur la page UlcoForum"
    
alldata :: (Html ()) -> Html ()
alldata menu = html_ $ do
    menu
    p_ $ toHtml $ T.pack "Toutes les données :"
    --li_ $ toHtml $ T.pack (show nameTopic)
    --li_ $ toHtml $ T.pack comment

alltopics:: (Html ()) -> [ForumTopic] -> Html ()
alltopics menu topics = html_ $ do
    menu
    p_ $ toHtml $ T.pack "Tous les topics :"
    mapM_ viewtopicslink topics

viewtopicslink :: ForumTopic -> Html()
viewtopicslink (ForumTopic id name) = do
    li_ $ a_ [href_ ("/onetopic/" <> showt id)] $ toHtml $ T.pack (show name)

dbgetalltopics:: Connection -> IO [ForumTopic]
dbgetalltopics conn = query_ conn "SELECT * FROM forumTopic"

--dbgettopicbyid:: Connection -> Int -> IO [ForumTopic]
--dbgettopicbyid conn id = query_ conn "SELECT * FROM forumTopic WHERE _ftid = ?" (Only id)

oneTopic :: (Html ()) -> [ForumMessage] -> ForumTopic -> Html ()
oneTopic menu messages (ForumTopic _ title) = html_ $ do
    menu
    h1_ $ toHtml title
    mapM_ viewmessage messages

dbgetallonetopic ::  Connection -> IO [ForumMessage]
dbgetallonetopic conn = do
    query_ conn "SELECT * FROM forumTopic"

viewmessage :: ForumMessage -> Html()
viewmessage (ForumMessage id ftid name message) = do
    li_ $ do
        toHtml name
        ": "
        toHtml $ message
