-- create database:
-- sqlite3 ulcoforum.db < ulcoforum.sql

-- Création de la table des différentes discussions --
CREATE TABLE forumTopic (
    _ftid INTEGER PRIMARY KEY AUTOINCREMENT,
    _ftname TEXT UNIQUE
);

-- Création de la table des messages des différentes discussions --
CREATE TABLE forumMessage (
    _fmid INTEGER PRIMARY KEY AUTOINCREMENT,
    _ftopicid INTEGER,
    _fmname TEXT,
    _fmmessage TEXT,
    FOREIGN KEY (_ftopicid) REFERENCES forumTopic(_ftid)
);

-- Insertion de 2 topic de forum --
INSERT INTO forumTopic (_ftname) VALUES("Le teletravail va-t-il continuer d exister apres la crise ?");
INSERT INTO forumTopic (_ftname) VALUES("L ete 2021 c est de la merde");

INSERT INTO forumMessage (_ftopicid, _fmname, _fmmessage) VALUES(1, "Florian", "Je pense, certaines entreprises trouvent que leurs employes sont plus performant avec");
INSERT INTO forumMessage (_ftopicid, _fmname, _fmmessage) VALUES(1, "Rio", "Miahou");
INSERT INTO forumMessage (_ftopicid, _fmname, _fmmessage) VALUES(1, "Baptisque", "Menfou");

INSERT INTO forumMessage (_ftopicid, _fmname, _fmmessage) VALUES(2, "Baptisque", "Wesh il fait que pleuvoir je peux pas boire mes bieres en terasse");
INSERT INTO forumMessage (_ftopicid, _fmname, _fmmessage) VALUES(2, "Florian", "Bien l'alcoolique....");
