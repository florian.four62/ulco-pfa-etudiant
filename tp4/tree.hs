data Tree a = Node a (Tree a) (Tree a) | Leaf 

mytree1 :: Tree Int
mytree1 = Node 7 (Node 2 Leaf Leaf)
                (Node 37 (Node 13 Leaf Leaf)
                         (Node 42 Leaf Leaf))

mytree2 :: Tree Double
mytree2 = Node 7 (Node 2 Leaf Leaf)
                (Node 37 (Node 13 Leaf Leaf)
                         (Node 42 Leaf Leaf))

instance Show a => Show (Tree a) where
    show Leaf = "_"
    show (Node e lt rt) = "(" ++ show e ++ show lt  ++  show rt ++ ")"

instance Foldable Tree where
    foldMap f Leaf = mempty
    foldMap f (Node x l r) = f x 
    
    
main :: IO ()
main = do
        print mytree1
        print mytree2
        -- print $ sum mytree1
        -- print $ maximum mytree1



