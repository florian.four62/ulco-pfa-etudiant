import Data.Char

-- isSorted
isSorted :: Ord a => [a] -> Bool
isSorted [] = True
isSorted [_] = True
isSorted (x0:x1:xs) = x0 < x1 && isSorted (x1:xs)

-- nocaseCmp 
nocaseCmp :: String -> String -> Bool
nocaseCmp strA strB =
    let
        lowerStrA = (map toLower strA)
        lowerStrB = (map toLower strB)
    in lowerStrA < lowerStrB

main :: IO ()
main = do
    print $ isSorted [1..4]
    print $ isSorted [4,3,1]
    print $ isSorted "abc"
    print $ isSorted "foo"
    print $ nocaseCmp "tata" "TOTO"
    

