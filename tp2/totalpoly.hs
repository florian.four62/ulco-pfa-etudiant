
-- safeTailString 
safeTailString :: [Char] -> [Char]
safeTailString "" = ""
safeTailString (_:t) = t

-- safeHeadString 
safeHeadString :: String -> Maybe Char
safeHeadString "" = Nothing
safeHeadString (x:_) = Just x

-- safeTail 
safeTail :: [a] -> [a]
safeTail [] = []
safeTail (_:t) = t

-- safeHead 
safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x:_) = Just x

main :: IO ()
main = do
    print $ safeTailString "test"
    print $ safeHeadString "test"
    print $ safeTail "test"
    print $ safeHead "test"

