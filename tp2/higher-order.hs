import Data.List

-- threshList
threshList :: Ord b => b -> [b] -> [b]
threshList s = map (min s)

-- selectList
selectList :: Ord a => a -> [a] -> [a]
selectList s = filter (<s)

-- maxList
maxList :: (Foldable t, Ord a) => t a -> a
maxList xs = foldr1 max xs


main :: IO ()
main = do
    print $ threshList 3 [1..5::Int]
    print $ threshList 'c' ['a'..'k']
    print $ selectList 3 [1..5::Int]
    print $ selectList 'c' ['a'..'k']
    print $ maxList [13,42,37::Int]
    print $ maxList ['a'..'k']


