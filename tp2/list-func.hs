
-- mymap1 
mymap1 :: (t -> a) -> [t] -> [a]
mymap1 _ [] = []
mymap1 f (x:xs) = f x : mymap1 f xs

-- mymap2
mymap2 :: (a -> b) -> [a] -> [b]
mymap2 f xs = go xs []
    where
        go [] acc = acc
        go (y:ys) acc = go ys (acc ++ [f y])

-- myfilter1 
myfilter1 :: (a -> Bool) -> [a] -> [a]
myfilter1 fn [] = []
myfilter1 fn (h:t)
    | (fn h)     = h : (myfilter1 fn t)
    | otherwise   = myfilter1 fn t

-- myfilter2 
myfilter2 :: (a -> Bool) -> [a] -> [a]
myfilter2 fn list =
    myfilter2_Internal list []
    where
        myfilter2_Internal [] acc = acc
        myfilter2_Internal (h:t) acc
            | (fn h)      = myfilter2_Internal t (acc ++ [h])
            | otherwise   = myfilter2_Internal t acc
            
-- myfoldl 

-- myfoldr 

main :: IO ()
main = putStrLn "TODO"

