{-# LANGUAGE OverloadedStrings #-}

import Data.Text (Text)
import Database.SQLite.Simple

selectAllArtists :: Connection -> IO[(Text, Text)]
selectAllArtists conn = query_ conn 
    "SELECT artist_name, title_name \
    \FROM title \
    \INNER JOIN artist ON title_artist = artist_id"

main :: IO()
main = withConnection "music.db" selectAllArtists >>= mapM_ print
