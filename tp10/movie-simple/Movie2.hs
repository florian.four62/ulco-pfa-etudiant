{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Movie2 where

import Data.Text (Text)
import Database.SQLite.Simple
import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)
import GHC.Generics (Generic)

data Movie = Movie 
    { movie_id      :: Int
    , movie_title   :: Text
    , movie_year    :: Int
    } deriving (Generic, Show)

data Prod = Prod 
    { _movie           :: Text
    , _year            :: Int
    , _person          :: Text
    , _role            :: Text
    } deriving (Generic, Show) 

instance FromRow Movie where
    fromRow = Movie <$> field <*> field <*> field 

instance FromRow Prod where
    fromRow = Prod <$> field <*> field <*> field <*> field 

dbSelectAllMovies :: Connection -> IO [Movie]
dbSelectAllMovies conn = query_ conn "SELECT * FROM movie"

dbSelectAllProds :: Connection -> IO [Prod]
dbSelectAllProds conn = query_ conn "SELECT movie_title as _movie, movie_year as _year, person_name as _person, role_name as _role FROM prod \
        \ INNER JOIN movie ON prod.prod_movie = movie.movie_id \
        \ INNER JOIN person ON prod.prod_person = person.person_id \
        \ INNER JOIN role ON prod.prod_role = role.role_id"

dbSelectMoviesFromPersonId :: Connection -> Int -> IO [Movie]
dbSelectMoviesFromPersonId conn person_id = query conn "SELECT DISTINCT movie.* FROM movie \
            \ INNER JOIN prod ON prod.prod_movie = movie.movie_id \
            \ WHERE prod.prod_person = ?" (Only person_id)
