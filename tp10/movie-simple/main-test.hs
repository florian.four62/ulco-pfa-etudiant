import Database.SQLite.Simple (open, close)

import Movie1
import Movie2

main :: IO ()
main = do
    conn <- open "movie.db"

    putStrLn "\nMovie1.dbSelectAllMovies"
    Movie1.dbSelectAllMovies conn >>= mapM_ print

    putStrLn "\nMovie1.dbSelectAllProds"
    Movie1.dbSelectAllProds conn >>= mapM_ print

    putStrLn "\nMovie1.dbSelectMoviesFromPersonId 1"
    Movie1.dbSelectMoviesFromPersonId conn 1 >>= mapM_ print

    putStrLn "\nMovie2.dbSelectAllMovies"
    Movie2.dbSelectAllMovies conn >>= mapM_ print

    putStrLn "\nMovie2.dbSelectAllProds"
    Movie2.dbSelectAllProds conn >>= mapM_ print

    putStrLn "\nMovie2.dbSelectMoviesFromPersonId 1"
    Movie2.dbSelectMoviesFromPersonId conn 1 >>= mapM_ print

    close conn