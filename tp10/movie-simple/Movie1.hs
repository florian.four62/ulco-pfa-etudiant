{-# LANGUAGE OverloadedStrings #-}

module Movie1 where

import Data.Text (Text)
import Database.SQLite.Simple

dbSelectAllMovies :: Connection -> IO [(Int,Text,Int)]
dbSelectAllMovies conn = query_ conn "SELECT * FROM movie"

dbSelectAllProds :: Connection -> IO [(Text,Int,Text,Text)]
dbSelectAllProds conn = query_ conn "SELECT movie_title, movie_year, person_name, role_name FROM prod \
            \ INNER JOIN movie ON prod.prod_movie = movie.movie_id \
            \ INNER JOIN person ON prod.prod_person = person.person_id \
            \ INNER JOIN role ON prod.prod_role = role.role_id"

dbSelectMoviesFromPersonId :: Connection -> Int -> IO [[Text]]
dbSelectMoviesFromPersonId conn person_id = query conn "SELECT DISTINCT movie_title FROM movie \
            \ INNER JOIN prod ON prod.prod_movie = movie.movie_id \
            \ WHERE prod.prod_person = ?" (Only person_id)