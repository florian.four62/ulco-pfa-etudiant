{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import GHC.Generics
import Lucid
import Data.Aeson hiding (json)
import Data.Maybe (fromMaybe)
import Network.Wai.Middleware.Gzip (gzip, def, gzipFiles, GzipFiles(..))
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import System.Environment (lookupEnv)
import Web.Scotty
import TextShow
import qualified Data.Text.Lazy as L

data Person = Person
    { _name :: L.Text
    , _year :: Int
    } deriving Generic

instance ToJSON Person

person :: Person
person = Person "toto" 42

myPage :: Html ()
myPage = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "Homepage"
        body_ $ do
            h1_ "This is the hompage"
            p_ "You'll find mutiple links above : "
            ul_ $ do
                li_ $ do
                    a_ [href_ "/route1"] "Route 1"
                li_ $ do    
                    a_ [href_ "/route1/route2"] "Route 1/2"
                li_ $ do    
                    a_ [href_ "/html1"] "Html 1"
                li_ $ do
                    a_ [href_ "/json1"] "Json 1"
                li_ $ do    
                    a_ [href_ "/json2"] "Json 2"
           

main :: IO ()
main = do
        port <- read . fromMaybe "3000" <$> lookupEnv "PORT"
        scotty port $ do
            middleware logStdoutDev
            middleware $ gzip def { gzipFiles = GzipCompress }
            middleware $ staticPolicy $ addBase "static"
            get "/" $ html $ renderText myPage
            get "/route1" $ html "Page route1 <img src='bob.png' />"
            get "/route1/route2" $ html "Page route1/route2 <img src='../bob.png' />"
            get "/html1" $ html "<p>This is a paragraph</p>"
            get "/json1" $ json ("toto"::String, 42::Int)
            get "/json2" $ json person
            get "/add1/:x/:y" $ do
                x <- param "x"
                y <- param "y"
                text $ showtl (x+y :: Int)
            get "/add2" $ json person
