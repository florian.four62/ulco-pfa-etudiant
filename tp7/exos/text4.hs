import qualified Data.Text.IO as T
import qualified Data.Text.Encoding as T
import qualified Data.ByteString.Char8 as C

main :: IO ()
main = do
    content <- T.readFile "text0.hs"
    C.putStrLn $ T.encodeUtf8 content
