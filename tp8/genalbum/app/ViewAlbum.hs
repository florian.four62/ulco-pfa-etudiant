{-# LANGUAGE OverloadedStrings #-}
module ViewAlbum where 

import Control.Monad
import Lucid
import Data.List
import qualified Data.Text as T

import Site

renderToFile :: FilePath -> Html a ->IO ()
renderToFile = Lucid.renderToFile

viewAlbum sites = do
    doctypehtml_ $ do
        head_ $ meta_ [charset_ "utf-8"]
        body_ $ do
            h1_ "Album"
            forM sites $ \s -> do
                h2_ $ toHtml $ url s
                div_ $ forM (images s) $ \i -> do
                    p_ $ toHtml i 
