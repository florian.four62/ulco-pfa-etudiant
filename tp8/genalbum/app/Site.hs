
{-# LANGUAGE OverloadedStrings #-}

module Site where 

import Data.Aeson
    ( eitherDecodeFileStrict', (.:), withObject, FromJSON(parseJSON) )

data Site = Site
    { images :: [String]
    , url :: String
    } deriving (Show)

instance FromJSON Site where 
    parseJSON = withObject "Site" $ \v -> do
        f <- v .: "imgs"
        l <- v .: "url"
        return $ Site f l

main :: IO ()
main = do
    putStrLn "this is genalbum"
    f <- eitherDecodeFileStrict' "../data/genalbum.json"
    print $ (f :: Either String [Site])
