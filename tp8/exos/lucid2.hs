{-# LANGUAGE OverloadedStrings #-}

import Lucid
import Data.Text.Lazy.IO as T

main :: IO()
main = do
    T.putStrLn $ renderText monHtml

monHtml :: Html ()
monHtml = do
    doctype_
    html_ $ do
        head_ (meta_ [charset_ "utf-8"] )
        body_ $ do
            h1_ "hello"
            img_ [src_ "toto.png"]
            p_ $ do
                "this is"
                a_ [href_ "toto.png"] ("a link")