{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Data.Text as T
import qualified Data.Aeson as A
import GHC.Generics

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    , address      :: Address
    } deriving (Generic, Show)

data Address = Address
    {
      road         :: T.Text
    , zipcode      :: Int
    , city         :: T.Text
    , number       :: Int   
    } deriving (Generic, Show)

instance A.ToJSON Person
instance A.ToJSON Address

persons :: [Person]
persons =
    [ Person "John" "Doe" 1970 (Address "Pont Vieux" 43000 "Espaly" 42)
    , Person "Haskell" "Curry" 1900 (Address "Pere Lachaise" 75000 "Paris" 1337)
    ]

main = do
    A.encodeFile "aeson3.json" (persons)