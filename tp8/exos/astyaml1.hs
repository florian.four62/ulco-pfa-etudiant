{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson as A
import Data.Yaml as Y
import Data.ByteString.Lazy.Char8 as LS
import Data.ByteString.Char8 as BS
import Data.HashMap.Strict

main :: IO ()
main = do
     LS.putStrLn $ A.encode $ A.String "john"
     LS.putStrLn $ A.encode $ A.Number 42
     LS.putStrLn $ A.encode $ A.Object $ fromList [("birthyear", A.Number 42)]
     LS.putStrLn $ A.encode $ A.Object $ fromList 
        [ ("birthyear", A.Number 42)
        , ("first", A.String "john")
        , ("last", A.String "doe")
        ]

     BS.putStrLn $ Y.encode $ Y.Object $ fromList 
        [ ("birthyear", Y.Number 42)
        , ("first", Y.String "john")
        , ("last", Y.String "doe")
        ]