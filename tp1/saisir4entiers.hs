import Control.Monad (forM_)
import System.IO (hFlush, stdout)
import Text.Read (readMaybe, Lexeme (String))

main :: IO ()
main = do
    forM_ [1..4] $ \i -> do
         putStr $ "saisie " ++ show i ++ " : "
         hFlush stdout
         result <- getLine
         let resultNombre = readMaybe result :: Maybe Int
         case resultNombre of
            Nothing -> putStrLn "-> saisie invalide"
            Just x -> putStrLn $ "-> vous avez saisi l'entier " ++ show x


