{-# OPTIONS_GHC -Wno-deferred-type-errors #-}

fizzbuzz1 :: [Int] -> [String]
fizzbuzz1 [] = []
fizzbuzz1 (x : xs) 
   | x `mod` 15 == 0        = "FizzBuzz" : fizzbuzz1 xs
   | x `mod` 3 == 0         = "Fizz" : fizzbuzz1 xs
   | x `mod` 5 == 0         = "Buzz" : fizzbuzz1 xs
   | otherwise              = show x : fizzbuzz1 xs

-- fizzbuzz2 :: [Int] -> [String]

-- fizzbuzz

main :: IO ()
main = do
    print $ fizzbuzz1[1..20]

