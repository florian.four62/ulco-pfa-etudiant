data Abr = Feuille | Noeud Int Abr Abr
    deriving (Show)

insererAbr :: Int -> Abr -> Abr
insererAbr x Feuille = Noeud x Feuille Feuille
insererAbr x (Noeud y ag ad) = 
    if x < y 
    then Noeud y (insererAbr x ag) ad
    else Noeud y ag (insererAbr x ad) 

listToAbr :: [Int] -> Abr
listToAbr = foldr insererAbr Feuille

abrToList :: Abr -> [Int]
abrToList Feuille = []
abrToList (Noeud x ag ad) = abrToList ag ++ [x] ++ abrToList ad

main :: IO ()
main = do
    print $ abrToList $ insererAbr 13 (insererAbr 3 (insererAbr 12 Feuille))
    print $ listToAbr [13, 3, 12]
    print $ abrToList $ listToAbr [13, 3, 12]